


export interface Mapper<M, E> {
    modelToEntity(model: M): E;
    entityToModel(entity: E): M;
}