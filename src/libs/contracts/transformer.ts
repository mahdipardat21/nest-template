



export abstract class Transformer<K, V> {
    abstract transform(data: K): V;

    public collection(data: Array<K>): Array<V> {
        return data.map(this.transform);
    }
}