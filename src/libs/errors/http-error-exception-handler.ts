import { ErrorHandler } from "./error-handler";

export class HTTPErrorExceptionHandler implements ErrorHandler<any> {
    handle() {
        throw new Error("Method not implemented.");
    }
}
