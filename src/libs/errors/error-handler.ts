




export interface ErrorHandler<T> {
    handle(): T
}