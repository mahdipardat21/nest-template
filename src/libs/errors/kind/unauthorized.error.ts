import { CustomError, ErrorMessage } from "../custom-error";
import { ERROR_EXCEPTION } from "../error-exceptions";




export class UnauthorizedError extends CustomError {
    public status = ERROR_EXCEPTION.UNAUTHORIZED_EXCEPTION;
    public message: string = 'unauthorized_ error';
    public errors: ErrorMessage[] = [{ context: 'auth', value: 'unauthorized' }];

}