import { CustomError, ErrorMessage } from "../custom-error";
import { ERROR_EXCEPTION } from "../error-exceptions";




export class UnauthenticatedError extends CustomError {
    public status = ERROR_EXCEPTION.UNAUTHENTICATED_EXEPTION;
    public message: string = 'unauthenticated_error';
    public errors: ErrorMessage[] = [{context: 'auth', value: 'unauthenticated'}];

}