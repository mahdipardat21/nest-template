import { CustomError, ErrorMessage } from "../custom-error";
import { ERROR_EXCEPTION } from "../error-exceptions";




export class NotFoundError extends CustomError {
    public status = ERROR_EXCEPTION.NOT_FOUND_EXCEPTION;
    public message: string = 'not_found';
    public errors: ErrorMessage[] = [];

    constructor(error: string) {
        super();
        this.errors.push({ context: 'error', value: error })
    }
}