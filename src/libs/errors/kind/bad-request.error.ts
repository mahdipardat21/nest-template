import { CustomError, ErrorMessage } from "../custom-error";
import { ERROR_EXCEPTION } from "../error-exceptions";



export class BadRequestError extends CustomError {
    public status = ERROR_EXCEPTION.BAD_REQUEST_EXCEPTION;
    public message: string = 'bad_request';
    public errors: ErrorMessage[];


    constructor(errors: Array<ErrorMessage>) {
        super();
        this.errors = errors;
    }

}