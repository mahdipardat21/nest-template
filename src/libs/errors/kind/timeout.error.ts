import { CustomError, ErrorMessage } from "../custom-error";
import { ERROR_EXCEPTION } from "../error-exceptions";




export class TimeoutError extends CustomError {
    public status = ERROR_EXCEPTION.TIMEOUT_EXCEPTION;
    public message: string = 'server_error';
    public errors: ErrorMessage[] = [{ context: 'error', value: 'timeout_service_error' }];

}