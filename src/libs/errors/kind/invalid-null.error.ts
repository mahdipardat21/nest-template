import { CustomError, ErrorMessage } from "../custom-error";
import { ERROR_EXCEPTION } from "../error-exceptions";




export class InvalidNullError extends CustomError {
    public status = ERROR_EXCEPTION.INVALID_NULL_EXCEPTION;
    public message: string = 'server_error';
    public errors: ErrorMessage[];

    constructor(errors: Array<ErrorMessage> | ErrorMessage | string) {
        super();

        if (Array.isArray(errors)) this.errors = errors;

        if (typeof errors === 'string') {
            this.errors = [{ context: 'error', value: errors }]
        }

        if (typeof errors === 'object' && errors.hasOwnProperty('context') && errors.hasOwnProperty('value')) {
            this.errors = [{ context: errors['context'], value: errors['value'] }]
        }

    }
}