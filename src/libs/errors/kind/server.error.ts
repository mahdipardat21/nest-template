import { CustomError, ErrorMessage } from "../custom-error";
import { ERROR_EXCEPTION } from "../error-exceptions";




export class ServerError extends CustomError {
    public status = ERROR_EXCEPTION.SERVER_ERROR_EXCEPTION;
    public message: string = 'server_error';
    public errors: ErrorMessage[] = [{ context: 'error', value: 'internal_server_error' }];
}