import { CustomError, ErrorMessage } from "../custom-error";
import { ERROR_EXCEPTION } from "../error-exceptions";




export class DisconnectedError extends CustomError {
    public status = ERROR_EXCEPTION.DISCONNECTED_EXCEPTION;
    public message: string = 'server_error';
    public errors: ErrorMessage[] = [{context: 'error', value: 'disconnected_driver'}];


}