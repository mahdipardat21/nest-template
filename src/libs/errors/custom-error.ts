import { ERROR_EXCEPTION } from './error-exceptions';

export interface ErrorMessage {
  context: string;
  value: string;
}

export abstract class CustomError extends Error {
  public abstract status: number | ERROR_EXCEPTION;
  public abstract message: string;
  public abstract errors: Array<ErrorMessage>;

  constructor() {
    super();
  }
}
