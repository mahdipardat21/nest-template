import { HttpStatus } from "@nestjs/common";
import { HttpErrorDto } from "./http-error.dto";
import { ResponseResultDto } from "./response-result.dto";




export class ResponseBuilder<T> {
    private _message: string = 'success';
    private _status: number = HttpStatus.OK;
    private _data: T;
    private _errors: Array<HttpErrorDto> = undefined;

    public message(value: string): ResponseBuilder<T> {
        this._message = value;
        return this;
    }

    public status(value: number): ResponseBuilder<T> {
        this._status = value
        return this;
    }

    public data(value: T): ResponseBuilder<T> {
        this._data = value;
        return this;
    }

    public errors(errors: Array<HttpErrorDto>): ResponseBuilder<T> {
        this._errors = errors;
        return this;
    }

    public build(): ResponseResultDto<T> {
        return new ResponseResultDto(this._message, this._status, this._data, this._errors)
    }
}