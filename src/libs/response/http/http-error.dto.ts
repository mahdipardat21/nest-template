import { ApiProperty } from "@nestjs/swagger";



export class HttpErrorDto {
    @ApiProperty({ type: String })
    context: string;


    @ApiProperty({ type: String })
    value: string;
}