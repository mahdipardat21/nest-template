import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { HttpErrorDto } from "./http-error.dto";





export class ResponseResultDto<T> {
    @ApiProperty({ type: String })
    message: string;

    @ApiProperty({ type: Number })
    status: number;

    data?: T;

    @ApiPropertyOptional({ type: [HttpErrorDto] })
    errors?: HttpErrorDto[];

    constructor(message: string, status: number, data: T, errors: HttpErrorDto[]) {
        this.message = message;
        this.status = status;
        this.data = data;
        this.errors = errors;
    }
}