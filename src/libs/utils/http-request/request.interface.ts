export interface IRequest {
  send<Response>(): Promise<Response>;
}
