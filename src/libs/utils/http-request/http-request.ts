import * as axios from 'axios';
import { IRequest } from './request.interface';

export enum METHOD {
  POST = 'post',
  GET = 'get',
  PUT = 'put',
  PATCH = 'patch',
  DELETE = 'delete',
}

export class HttpRequest<T> implements IRequest {
  private readonly _httpClient = axios.default.create();

  private _method: METHOD;
  private _url: string;
  private _headers: Record<string, string>;
  private _data: T;

  constructor(
    method: METHOD,
    url: string,
    headers: Record<string, string>,
    data: T,
  ) {
    this._method = method;
    this._url = url;
    this._headers = headers;
    this._data = data;
  }

  async send<Response>(): Promise<Response> {
    const response = await this._httpClient.request({
        method: this._method,
        data: this._data,
        url: this._url,
        headers: this._headers
    });

    return response.data;
  }
}
