import { HttpRequest, METHOD, IRequest } from './';

export class HttpRequestBuilder<T> {
  private _method: METHOD = METHOD.GET;
  private _url: string;
  private _headers: Record<string, string> = {};
  private _data: T = null;

  public setMethod(method: METHOD): HttpRequestBuilder<T> {
    this._method = method;
    return this;
  }

  public setAuth(token: string): HttpRequestBuilder<T> {
    this._headers['Authorization'] = token;
    return this;
  }

  public setUrl(url: string): HttpRequestBuilder<T> {
    this._url = url;
    return this;
  }

  public setHeader(key: string, value: string): HttpRequestBuilder<T> {
    this._headers[key] = value;
    return this;
  }

  public setData(data: T): HttpRequestBuilder<T> {
    this._data = data;
    return this;
  }

  public build(): IRequest {
    return new HttpRequest<T>(
      this._method,
      this._url,
      this._headers,
      this._data,
    );
  }
}
