import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsOptional, IsString, IsNumber } from 'class-validator';

export enum SortQuery {
    Desc = 'desc',
    Asc = 'asc'
}


export class PaginateQueryDto {
    @ApiPropertyOptional({ type: String })
    @IsOptional()
    @IsString()
    q: string;

    @ApiPropertyOptional({ type: Number })
    @IsOptional()
    @IsNumber()
    page: number;


    @ApiPropertyOptional({ type: Number })
    @IsOptional()
    @IsNumber()
    limit: number;


    @ApiPropertyOptional({ enum: SortQuery })
    @IsOptional()
    @IsNumber()
    sort: SortQuery
}