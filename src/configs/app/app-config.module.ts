import { Module } from "@nestjs/common";
import { ConfigModule, registerAs } from "@nestjs/config";
import * as Joi from 'joi';
import { AppConfigService } from "./app-config.service";

const config = registerAs('app', () => ({
    port: +process.env.PORT
}))

@Module({
    imports: [
        ConfigModule.forRoot({
            envFilePath: '.env',
            load: [config],
            validationSchema: Joi.object({
                PORT: Joi.number().required()
            })
        })
    ],
    providers: [AppConfigService],
    exports: [AppConfigService]
})
export class AppConfigModule {}