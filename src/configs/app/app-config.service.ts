import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";



@Injectable()
export class AppConfigService {

    constructor(private readonly _configService: ConfigService) { }

    public get port(): number {
        return this._configService.get<number>('app.port');
    }
}