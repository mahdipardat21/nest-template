import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import { globalMiddlewares } from './boot/middlewares';
import { SwaggerCreator } from './boot/swagger';
import { AppConfigService } from './configs/app/app-config.service';

(async () => {
  const app: NestExpressApplication =
    await NestFactory.create<NestExpressApplication>(AppModule);

  // application config env
  const config = app.get<AppConfigService>(AppConfigService);


  globalMiddlewares(app);


  app.useGlobalPipes(new ValidationPipe({
    forbidNonWhitelisted: true,
    // set configs
  }));


  // generate swaggers documents
  const swagger = new SwaggerCreator([
    {
      modules: [AppModule],
      name: 'AppController',
      description: 'Main App API Documentation',
      version: '1.0.0'
    }
  ], app);
  swagger.build();

  app.listen(config.port);
})();
