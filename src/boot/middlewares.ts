import { NestExpressApplication } from '@nestjs/platform-express';
import { rateLimit } from 'express-rate-limit';

export const globalMiddlewares = (app: NestExpressApplication): void => {


  app.use(
    rateLimit({
      windowMs: 1000 * 60 * 60,
      max: 100000,
      message: 'Too many request created from this IP',
    }),
  );

  
};
