import { ConsoleLogger } from '@nestjs/common';
import { NestExpressApplication } from '@nestjs/platform-express';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

export interface CreatorOptions {
  name: string;
  modules: Array<Function>;
  description?: string;
  version?: string;
}

export class SwaggerCreator {
  private _app: NestExpressApplication;
  private _modules: Array<CreatorOptions> = [];

  constructor(modulesOptions: Array<CreatorOptions>, app: NestExpressApplication) {
    this._modules = modulesOptions;
    this._app = app;
  }

  public build(): void {
    for (const options of this._modules) {
      SwaggerModule.setup(
        `/swagger/${options.name + (options.version ? '/' + options.version : '')
        }`,
        this._app,
        SwaggerModule.createDocument(
          this._app,
          new DocumentBuilder()
            .setTitle(`KYC ${options.name} ${options.version}`)
            .setDescription(options.description || `KYC ${options.name} API`)
            .setVersion(options.version || '1.0.0')
            .addBearerAuth()
            .build(),
        ),
      );

      const logger = new ConsoleLogger();
      logger.log(
        `KYC Swagger ${options.name} ${options.version || ''} GENERATED`,
        SwaggerCreator.name,
      );
    }
  }
}
