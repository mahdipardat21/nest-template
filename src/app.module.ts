import { Module } from "@nestjs/common";
import { AppConfigModule } from "./configs/app/app-config.module";



@Module({
    imports: [AppConfigModule]
})
export class AppModule { }